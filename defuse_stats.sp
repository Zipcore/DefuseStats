#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <multicolors>

#define PL_VERSION "1.0"

public Plugin myinfo = 
{
	name        = "Defuse Abort Stats",
	author		= ".#Zipcore",
	description = "",
	version     = PL_VERSION,
	url         = "zipcore#googlemail.com"
};

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

float g_fC4Timer;
Handle g_hC4Timer = null;

bool g_bParty;
Handle g_hParty = null;

float g_fBombPlanted;

public void OnPluginStart()
{
	HookEventEx("bomb_abortdefuse", Event_OnAbortPlant);
	HookEvent("bomb_planted", Event_BombPlanted, EventHookMode_Pre);
	HookEvent("bomb_defused", Event_BombDefused, EventHookMode_Pre);
	
	g_hC4Timer = FindConVar("mp_c4timer");
	g_fC4Timer = GetConVarFloat(g_hC4Timer);
	HookConVarChange(g_hC4Timer, Action_OnSettingsChange);
	
	g_hParty = FindConVar("sv_party_mode");
	g_bParty = GetConVarBool(g_hC4Timer);
	HookConVarChange(g_hParty, Action_OnSettingsChange);
}

public void Action_OnSettingsChange(Handle cvar, const char[] oldvalue, const char[] newvalue)
{
	if (cvar == g_hC4Timer)
		g_fC4Timer = StringToFloat(newvalue);
	else if (cvar == g_hParty)
		g_bParty = view_as<bool>(StringToInt(newvalue));
}

public Action Event_OnAbortPlant(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	int c4 = FindEntityByClassname(MaxClients+1, "planted_c4");
	if(c4 == -1)
		return Plugin_Continue;
	
	float fTimeLeft = GetEntPropFloat(c4, Prop_Send, "m_flDefuseCountDown") - GetGameTime();
	
	if(!IsPlayerAlive(client))
		CPrintToChatAll("{darkred}%N{green} was killed while defusing the bomb, {darkred}%.2fs{green} were still left to finish the job.", client, fTimeLeft);

	return Plugin_Continue;
}

public Action Event_BombPlanted(Event event, const char[] name, bool dontBroadcast)
{
	g_fBombPlanted = GetGameTime();
	
	return Plugin_Continue;
}

public Action Event_BombDefused(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	
	float fTimeLeft = (g_fBombPlanted + g_fC4Timer) - GetGameTime();
	
	if(fTimeLeft < 1.0 && !g_bParty)
	{
		SetConVarBool(g_hParty, true);
		CreateTimer(0.0, Timer_DisablePartyMode);
	}
		
	CPrintToChatAll("{darkred}%N{green} defused the bomb {darkred}%.2fs{green} before it would have been exploded.", client, fTimeLeft);
	
	return Plugin_Continue;
}

public Action Timer_DisablePartyMode(Handle timer, any data)
{
	SetConVarBool(g_hParty, false);
}